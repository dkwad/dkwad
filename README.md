---
DKwad
The SoftClass Dispatcher
---


**<u>Summary:</u>**

* 1/ [The SoftClass](#The_SoftClass)
* 2/ [The Entities](#The_Entities)
* 3/ [Kattrs](#Kattrs)
* 4/ [Note about Kikonf](#Note_about_Kikonf)
  
**dKwad** is a Generic Configuration Management System for Linux Debian.  
Generic because its purpose is to manage any software that runs in Linux Debian.  
  
They are two main concepts:  
  
**1/ [The SoftClass](#top)**  
  
A SoftClass is a program who:
* Configures one and only one specific Software (e.g.: Apache &#174;, Tomcat &#174;, Openssl &#174;, PostgreSQL &#174; ApacheMQ &#174;, KafKa &#174;, K8S &#174;, ...).
* Reads its own configuration from 2 files:
  * A structured file (xml, yaml, or hcl are supported) called **Action file (or SoftClass file)** with its expected attributes.  
    This file has a special tag: "**output**", describing what type of output the program will produce.
  * A file called **kattrs**, with the endpoints of the Software it is targeting.

  
SoftClasses are grouped into **plugins** are live into the <dkwad\_home>/plugins directory.  
  
<u>SoftClasses are organized as:</u>  

* **Category** (e.g.: middleware, cloud)
* **Software** (e.g.: apache, tomcat)
* **byWho** (by johnDo)
* **SoftClass** (name of the SoftClass, e.g.: myBackupSC myRestoreSC myInstallSC myAddAdminUserSC )
Published SoftClass are and must be OSI Open Source,  
They may manage proprietary Software but if they are Published they must be under an OSI Open Source licences compatible with GNU GPL3.  
  
  
**2/ [The Entities](#top)**  
  
The Supported Entities in hierarchical order are:  
(The entities beetween \[\] are optional.)  
  

* **Domain**
* **Environment** *(env)*
* **Hosted Client** *(hco, maybe local)*
* \[**Sub Hosted Client** *(shco)*\]
* **Application** *(app)*
* \[**Sub Application** *(sapp)*\]
* **Application Instance** *(appinst)*
* \[**Cloud Provider** *(cloudp)*\]
* \[**Cloud Instance** *(cloudi)*\]
* **Machine**
Nothing happens without the **Application**, an Application is defined for a specific Hosted Client (and SubHostedClient).  
  
Nothing would run without an **Application Instance** an Application instance is the expression of  
one Application for a Domain/Environment/HostedClient/\[SubHostedClient\].  
  
The SoftClass runner is called with an Application Instance and a set of machine for wich the appinst is allowed to.  
  
  
**3/ [Kattrs](#top)**  
  
Kattrs is deduced and declined from the Entities hierarchy:  
For reminder the Enitity hierarchy is:  
  

* **Domain**  
  &#x27A4; kattrs.xml
* **Environment** *(env)*  
  &#x27A4; kattrs.xml
* **Hosted Client** *(hco, maybe local)*  
  &#x27A4; kattrs.xml
* \[**Sub Hosted Client** *(shco)*  
  &#x27A4; kattrs.xml \]
* **Application** *(app)*  
  &#x27A4; kattrs.xml
* \[**Sub Application** *(sapp)*  
  &#x27A4; kattrs.xml \]
* **Application Instance** *(appinst)*  
  &#x27A4; kattrs.xml
* \[**Cloud Provider** *(cloudp)*  
  &#x27A4; kattrs.xml \]
* \[**Cloud Instance** *(cloudi)*  
  &#x27A4; kattrs.xml \]
* **Machine**  
  &#x27A4; kattrs.xml
Each level may contain its own kattrs.  
  
Actually the resulting kattrs sent to the SoftClass is declined from the merge of all kattrs found in the hierarchy.  
They are merged on the the software name with a priority for the lowest level, because the merge is bottom up.  
  
  
**4/ Note about [Kikonf](https://www.kikonf.org)**

**[Kikonf](https://www.kikonf.org) borned in 2003**, was:  
  
* The **first to create** the concept of: **Uniform Platform to administrate all MiddleWare Software** publics and privates.  
  This may ring bells.  
  This concept was copied by many and very well known Fellows.
* The **first to create** the concept of: **plugins** dedicated to the **configuration of a specific Software**.  
  This concept was copied by many and very well known Fellows.
* The **first to create** the concepts of: **Action Class**  
  the short program (within the plugin) who will run a specific Action on the backend Software.  
  Copied by well known Fellows.
* The **first to create** the concepts of: **Operation on Action Classes**  
  See: [kikact.pdf](https://www.kikonf.org/doc/kikact.pdf) *Chapter: 3.4 OPERATIONS*  
  
  The five Operations supported by each Action class:  

  * **inject**
  * **inrun**
  * **run**
  * **extract**
  * **remove**
  
  Very similary with what others may later name crud isn't ?  
  Also Copied by well known Fellows.
* The creator of the concept of **Descriptor file**,  
  the xml file who describes the types of the parameters expected by the Action class.  
  Copied by well known Fellows.
* The First to create the concept of the **merging of Action classes**, in one big file called the "Custom Action file". To run a global **operation** on the Architecture. Hence this file would be called for a specific operation. See: [Set of Actions at: http://kikonf.org/overview.en.html.](http://kikonf.org/overview.en.html)*Here the set of Actions are in my.xml:
  * Calling for **inject operation** kikarc my.xml
  * Calling for **extract operation** kikarc my.xml -o extract
  * Calling for **remove operation** kikarc my.xml -o remove* Deeply copied by well known fellows. This also might ring some bells.

dKwad **inpired from Kikonf** now goes **farther** !
---
Trademarks :

* "Openssl" is a trademark or registered trademark of the OpenSSL Software Foundation.
* "Apache, Apache Tomcat, "Tomcat", ApacheMQ and KafKa" and are trademarks of the Apache Software Foundation.
* "PostgreSQL" is a trademark or registered trademark of Community Association of Canada.
* "K8S", "kubernetes" is a trademark or registered trademark of The Linux Foundation .
* Other names may be trademarks of their respective owners.
  
Copyright &copy; 2024 - Patrick Germain Placidoux
